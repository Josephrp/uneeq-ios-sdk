//
//  AppDelegate.swift
//  DigitalHumanSDK-Dev-Test
//
//  Created by David Wood on 7/08/19.
//  Copyright © 2019 Paperkite. All rights reserved.
//

import UIKit
import DigitalHumanSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    var window: UIWindow?
    
    lazy var digitalHumanSDK: DigitalHumanSDK = {
        // Initializing with a client secret
        let digitalHumanSDK = self.createSDKWithClientSecret()
        
        digitalHumanSDK.loggingLevel = .verbose
        digitalHumanSDK.messageDelegate = self
        
        return digitalHumanSDK
    }()
    
    // Example of creation of the SDK object with a clientSecret
    func createSDKWithClientSecret() -> DigitalHumanSDK {
        let options = DHOptions(workspaceId: "A-WORKSPACE-ID",
                                clientSecret: "A-CLIENT-SECRET",
                                url: "AREGIONURL")
        

        let digitalHumanSDK = DigitalHumanSDK(options: options)
        return digitalHumanSDK
    }
    
    // Example of creation of the SDK object with a tokenId
    func createSDKWithToken(_ tokenId: String) -> DigitalHumanSDK {
        let tokenOptions = DHTokenOptions(workspaceId: "A-WORKSPACE-ID",
                                tokenId: tokenId,
                                url: "AREGIONURL")

        
        let digitalHumanSDK = DigitalHumanSDK(tokenOptions: tokenOptions)
        return digitalHumanSDK
    }

    // Delegates
    weak var debugDelegate: AppDebugDelegate?
    weak var callStatusDelegate: AppCallStatusDelegate?
    
    // Logging variabless
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        return dateFormatter
    }()

    var debugLog: String = ""


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// MARK: - Receive DHMessages from the SDK and send back actions and information to the app via delegates
extension AppDelegate: DHMessageDelegate {
    func digitalHumanMessage(_ message: DHMessage) {
        switch message {
        case .avatarAvailable:
            self.callStatusDelegate?.statusMessage("avatar available")
        case .avatarUnavailable:
            self.callStatusDelegate?.statusMessage("avatar unavailable")
            self.callStatusDelegate?.callStatus(false)
        case .sessionLive:
            self.callStatusDelegate?.callStatus(true)
        case .micActivity(let level):
            // If you had your own custom mic activity view you could drive it from this level value
            print("Mic level:\(level)")
        default:
            print("No action for this message: \(message) in AppDelegate")
        }

        let msg = self.dateFormatter.string(from: Date()) + " \(message)\n"
        self.debugLog = self.debugLog + msg
        self.debugDelegate?.debugMessage(msg)
    }
}

// MARK: - AppDelegate delegate protocols
protocol AppDebugDelegate: class {
    func debugMessage(_ msg: String)
}

protocol AppCallStatusDelegate: class {
    
    /// Communicates if we have a call connected to the Digital Human Service or not
    ///
    /// - Parameter enabled: call enabled true/false
    func callStatus(_ enabled: Bool)
    
    /// Communicates status messages from the Digital Human service to the app
    ///
    /// - Parameter msg: message sent
    func statusMessage(_ msg: String)
}

extension AppCallStatusDelegate {
    /// Default implementation to make this optional in delegates
    func statusMessage(_ msg: String) {
        print("default status message call to make this function optional")
    }
}

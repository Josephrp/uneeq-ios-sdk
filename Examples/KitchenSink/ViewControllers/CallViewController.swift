//
//  ViewController.swift
//  DigitalHumanSDK-Dev-Test
//
//  Created by David Wood on 7/08/19.
//  Copyright © 2019 Paperkite. All rights reserved.
//

import Foundation
import UIKit
import DigitalHumanSDK
import AVFoundation

class CallViewController: UIViewController {
    // MARK: - SDK dependency
    var digitalHumanSDK: DigitalHumanSDK? = {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            return appDelegate.digitalHumanSDK
        } else {
            return nil
        }
    }()

    // MARK: - Outlets
    @IBOutlet weak var replyLabel: UILabel!
    @IBOutlet weak var remoteContainer: UIView!
    @IBOutlet weak var localContainer: UIView!
    
    @IBOutlet weak var micActivityContainer: UIView!
    
    @IBOutlet weak var pauseResumeButton: UIButton!
    @IBOutlet weak var pushToSpeakButton: UIButton!
    
    // MARK: - View state properties
    var speakerOn: Bool = true {
        didSet {
            switch self.speakerOn {
            case false:
                self.digitalHumanSDK?.speakerOff()
            case true:
                self.digitalHumanSDK?.speakerOn()
            }
        }
    }
    
    var mute: Bool = false {
        didSet {
            switch self.mute {
            case false:
                self.digitalHumanSDK?.unmuteAudio()
            case true:
                self.digitalHumanSDK?.muteAudio()
            }
        }
    }
    
    var pauseSession: Bool = false {
        didSet {
            switch self.pauseSession {
            case false:
                self.digitalHumanSDK?.resumeSession()
                self.pauseResumeButton.setTitle("Pause Session", for: .normal)
            case true:
                self.digitalHumanSDK?.pauseSession()
                self.pauseResumeButton.setTitle("Resume Session", for: .normal)
            }
        }
    }
    
    // MARK: - View overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add actions

        self.pauseResumeButton.addTarget(self, action: #selector(self.pauseResumeDidTap), for: .touchUpInside)
        
        // Configure initial state
        self.speakerOn = true
        self.mute = false
        
        self.addRemotePreviewView()
        self.addLocalPreviewView()
        self.addMicActivityView()

        self.digitalHumanSDK?.avatarDelegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.navigationController?.topViewController != self {
            self.digitalHumanSDK?.endSession()
        }
    }
    
    /// Adds the remote preview view to the screen, received from the DigitalHumanSDK
    func addRemotePreviewView() {
        guard let view = self.digitalHumanSDK?.getRemoteVideoView(frame: self.remoteContainer.frame) else { return }
        
        self.remoteContainer.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: self.remoteContainer.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.remoteContainer.trailingAnchor),
            view.topAnchor.constraint(equalTo: self.remoteContainer.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.remoteContainer.bottomAnchor)
            ])
    }

    /// Adds the local preview view to the screen, received from the DigitalHumanSDK
    func addLocalPreviewView() {
        guard let view = self.digitalHumanSDK?.getLocalVideoPreviewView(frame: self.localContainer.frame) else { return }

        self.localContainer.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: self.localContainer.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.localContainer.trailingAnchor),
            view.topAnchor.constraint(equalTo: self.localContainer.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.localContainer.bottomAnchor)
            ])
    }
    /// Adds the local preview view to the screen, received from the DigitalHumanSDK
    func addMicActivityView() {
        guard let view = self.digitalHumanSDK?.getMicActivityView(frame: self.micActivityContainer.frame, levels: 5, spacing: 8)  else { return }

        self.micActivityContainer.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: self.micActivityContainer.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.micActivityContainer.trailingAnchor),
            view.topAnchor.constraint(equalTo: self.micActivityContainer.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.micActivityContainer.bottomAnchor)
            ])
    }
    
    // MARK: - Actions
    @IBAction func optionsDidTap(_ sender: UIButton) {
        self.showOptions()
    }
    
    @IBAction func audioButtonOn(_ sender: UIButton) {
        self.digitalHumanSDK?.startRecording()
    }
    
    @IBAction func audioButtonOff(_ sender: UIButton) {
        self.digitalHumanSDK?.stopRecording()
    }
    
    @IBAction func pauseResumeDidTap() {
        self.pauseSession.toggle()
    }
    
    @IBAction func toggleMicActivity(_ sender: Any) {
        
        guard let micActivitySwitch = sender as? UISwitch else { return }
        
        self.micActivityContainer.isHidden = !micActivitySwitch.isOn
        self.digitalHumanSDK?.micActivityMessages = micActivitySwitch.isOn
    }
}

extension CallViewController: DHTextDelegate {
    /// Updates the reply label with the avatar text received
    ///
    /// - Parameter text: avatar text
    func digitalHumanTextReceived(_ text: String) {
        self.replyLabel.text = text
    }
}

extension CallViewController {
    func showOptions() {
        let alertController = UIAlertController(title: "Test options", message: nil, preferredStyle: .actionSheet)
        
        let welcomeAction = UIAlertAction(title: "Send welcome", style: .default) { [weak self] (action) in
            self?.digitalHumanSDK?.callWelcome()
        }

        let speakerAction = UIAlertAction(title: "Turn speakers " + self.textForBoolean(self.speakerOn), style: .default) { [weak self] (action) in
            self?.speakerOn.toggle()
        }
        
        let muteAction = UIAlertAction(title: "Turn mute " + self.textForBoolean(self.mute), style: .default) { [weak self] (action) in
            self?.mute.toggle()
        }
        
        let webRTCStatsAction =  UIAlertAction(title: "Request WebRTC stats", style: .default) { [weak self] (action) in
            self?.digitalHumanSDK?.requestWebRTCStats()
        }
        
        let flipCameraAction =  UIAlertAction(title: "Rotate camera", style: .default) { [weak self] (action) in
            self?.digitalHumanSDK?.flipCameraPreview()
        }
        
        let selectFrontCameraAction =  UIAlertAction(title: "Select front camera", style: .default) { [weak self] (action) in
            self?.selectFrontCamera()
        }
        
        let selectBackCameraAction =  UIAlertAction(title: "Select rear camera", style: .default) { [weak self] (action) in
            self?.selectBackCamera()
        }
        
        let sendTranscriptAction = UIAlertAction(title: "Send transcript", style: .default) { [weak self] (action) in
            self?.presentSendTranscript()
        }
        
        let listAvailableInputs =  UIAlertAction(title: "List audio inputs", style: .default) { [weak self] (action) in
            self?.presentSelectMicInputs()
        }
        
        let listAvailableOutputs =  UIAlertAction(title: "List audio outputs", style: .default) { [weak self] (action) in
            self?.presentSelectAudioOutputs()
        }
        
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        alertController.addAction(welcomeAction)
        alertController.addAction(speakerAction)
        alertController.addAction(muteAction)
        alertController.addAction(webRTCStatsAction)
        alertController.addAction(flipCameraAction)
        alertController.addAction(selectFrontCameraAction)
        alertController.addAction(selectBackCameraAction)
        alertController.addAction(sendTranscriptAction)
        alertController.addAction(listAvailableInputs)
        alertController.addAction(listAvailableOutputs)

        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textForBoolean(_ bool: Bool) -> String {
        if bool == false {
            return "on"
        } else {
            return "off"
        }
    }
}

// MARK: - Camera operations
extension CallViewController {
    func flipCameraPreview() {
        self.digitalHumanSDK?.flipCameraPreview()
    }
    
    func selectFrontCamera() {
        self.digitalHumanSDK?.setCamera(.front)
    }
    
    func selectBackCamera() {
        self.digitalHumanSDK?.setCamera(.back)
    }
}

// MARK: - Direct text sending to the SDK
extension CallViewController {
    func presentSendTranscript() {
        let alertController = UIAlertController(title: "Enter text to send", message: nil, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter text here"
        }
        
        
        let sendAction = UIAlertAction(title: "Send", style: .default) { [weak self] action in
            if let text = alertController.textFields?.first?.text {
                self?.digitalHumanSDK?.sendTranscript(text: text)
            }
        }
        
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Microphone input selection
extension CallViewController {
    func presentSelectMicInputs() {
        let currentInput = self.digitalHumanSDK?.getCurrentMicInput()
        
        let alertController = UIAlertController(title: "Select input", message: nil, preferredStyle: .alert)
        
        self.digitalHumanSDK?.getMicInputsAvailable().forEach({ input in
            var title: String = input.portName
            
            if input.uid == currentInput?.uid {
                title = title + " (selected)"
            }
            
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] action in
                self?.digitalHumanSDK?.setMicInput(input)
            })
            
            alertController.addAction(action)
        })
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Audio output selection
extension CallViewController {
    func presentSelectAudioOutputs() {
        let currentOutput = self.digitalHumanSDK?.getCurrentAudioOutput()
        
        let alertController = UIAlertController(title: "Select output", message: nil, preferredStyle: .alert)
        
        self.digitalHumanSDK?.getAudioOutputs().forEach({ output in
            var title: String = output.portName
            
            if output.uid == currentOutput?.uid {
                title = title + " (selected)"
            }
            
            let action = UIAlertAction(title: title, style: .default, handler: { [weak self] action in
                self?.digitalHumanSDK?.setAudioOutput(output)
            })
            
            alertController.addAction(action)
        })
        let cancelAction =  UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

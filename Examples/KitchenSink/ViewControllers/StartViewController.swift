//
//  StartViewController.swift
//  DigitalHumanSDK-Dev-Test
//
//  Created by David Wood on 13/08/19.
//  Copyright © 2019 Paperkite. All rights reserved.
//

import UIKit
import DigitalHumanSDK
import AVFoundation

class StartViewController: UIViewController {
    
    // MARK: - SDK dependency
    var digitalHumanSDK: DigitalHumanSDK? = {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            return appDelegate.digitalHumanSDK
        } else {
            return nil  
        }
    }()

    // MARK: - Outlets
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var verboseLoggingSwitch: UISwitch!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    // MARK: - State
    var loading: Bool = false {
        didSet {
            if self.loading == true {
                self.activitySpinner.startAnimating()
            } else {
                self.activitySpinner.stopAnimating()
            }
        }
    }
    
    var verbosity: Bool = false {
        didSet {
            self.verboseLoggingSwitch.isOn = self.verbosity
            
            if self.verbosity == true {
                self.digitalHumanSDK?.loggingLevel = .verbose
            } else {
                self.digitalHumanSDK?.loggingLevel = .error
            }
        }
    }
    
    // MARK: - View overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Default setting
        self.verbosity = false
        
        // Check permissions
        self.checkPermissions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.callStatusDelegate = self
        }
    }
    
    // MARK: - Actions
    @IBAction func startSession(_ sender: Any) {
        guard self.loading == false else { return }
        self.digitalHumanSDK?.startSession(sessionId: UUID().uuidString)
        self.loading = true
    }
    
    @IBAction func cancelSession(_ sender: Any) {
        self.digitalHumanSDK?.endSession()
        self.loading = false
    }

    @IBAction func toggleVerbosity(_ sender: Any) {
        self.verbosity.toggle()
    }
}

extension StartViewController: AppCallStatusDelegate {
    func callStatus(_ enabled: Bool) {
        if enabled == true {
            self.performSegue(withIdentifier: "showCall", sender: self)
            self.loading = false
            self.statusLabel.text = ""
        } else {
            print("Call not started")
            self.loading = false
        }
    }
    
    func statusMessage(_ msg: String) {
        self.statusLabel.text = msg
    }
}

// MARK: - Permissions checking
extension StartViewController {
    func checkPermissions() {
        // Microphone permission
        if AVAudioSession.sharedInstance().recordPermission == .undetermined {
            // Request permission
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        }
        
        // Camera permission
        if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            })
        }
    }
}

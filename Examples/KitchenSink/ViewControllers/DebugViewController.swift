//
//  DebugViewController.swift
//  DigitalHumanSDK-Dev-Test
//
//  Created by David Wood on 13/08/19.
//  Copyright © 2019 Paperkite. All rights reserved.
//

import UIKit
import DigitalHumanSDK

class DebugViewController: UIViewController {

    // MARK: - IB Outlets
    @IBOutlet weak var textView: UITextView!
    
    // MARK: - Dependencies
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            // Get debug log so far
            self.textView.text = appDelegate.debugLog

            appDelegate.debugDelegate = self
        }
    }
    
    @IBAction func clearTextView() {
        self.textView.text = ""
    }
    
    @IBAction func displayShareSheet() {
        let activityViewController = UIActivityViewController(activityItems: [self.textView.text], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: {})
    }
}

extension DebugViewController: AppDebugDelegate {
    func debugMessage(_ msg: String) {
        self.textView.text = self.textView.text + msg
    }
}

// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.2 (swiftlang-1100.0.278 clang-1100.0.33.9)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name DigitalHumanSDK
import AVFoundation
import Alamofire
import CommonCrypto
import CoreVideo
@_exported import DigitalHumanSDK
import Foundation
import StompClientLib
import Swift
import UIKit
import WebRTC
public enum DHRTCConfig : Swift.Int {
  case passThru
  case answerFirst
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
extension DigitalHumanSDK {
  public func getMicActivityView(frame: CoreGraphics.CGRect, levels: Swift.Int = 5, spacing: CoreGraphics.CGFloat = 8, baseColor: UIKit.UIColor = .gray, fillColor: UIKit.UIColor = .green) -> UIKit.UIView?
}
public enum DHLoggingLevel : Swift.Int {
  case disabled
  case warning
  case error
  case verbose
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
public struct DHOptions {
  public init(workspaceId: Swift.String, clientSecret: Swift.String, url: Swift.String, customData: [Swift.String : Swift.String]? = nil, sendLocalVideo: Swift.Bool = true, vadEnabled: Swift.Bool = false)
}
public struct DHTokenOptions {
  public init(workspaceId: Swift.String, tokenId: Swift.String, url: Swift.String, customData: [Swift.String : Swift.String]? = nil, sendLocalVideo: Swift.Bool = true, vadEnabled: Swift.Bool = false)
}
public struct DHError : Swift.Error {
  public var localizedDescription: Swift.String {
    get
  }
}
public protocol DHTextDelegate : AnyObject {
  func digitalHumanTextReceived(_ text: Swift.String)
}
public protocol DHMessageDelegate : AnyObject {
  func digitalHumanMessage(_ message: DigitalHumanSDK.DHMessage)
}
public enum DHMessage {
  case avatarAnswer(answer: Swift.String, answerAvatar: Swift.String, answerSpeech: Swift.String)
  case avatarAvailable
  case avatarQuestionText(question: Swift.String)
  @available(*, deprecated, renamed: "finishedSpeaking", message: "Deprecated in 1.3.4, use .finishedSpeaking instead")
  case avatarTextInputFinished
  case finishedSpeaking
  case avatarUnavailable
  case avatarAnswerContent(content: Swift.String)
  case avatarAnswerHTML(html: Swift.String)
  case avatarRequestCompleted
  @available(*, deprecated, renamed: "startedSpeaking", message: "Deprecated in 1.3.4, use .startedSpeaking instead")
  case avatarTextInputStarted
  case startedSpeaking
  case deviceError(error: Swift.Error)
  case deviceSetCameraSuccess(position: AVFoundation.AVCaptureDevice.Position)
  case deviceSetMicSuccess(audioInputDevice: AVFoundation.AVAudioSessionPortDescription)
  case deviceCameraPositionNotFoundError(position: AVFoundation.AVCaptureDevice.Position)
  case deviceAudioInputDeviceNotFound(input: AVFoundation.AVAudioSessionPortDescription)
  case setAudioOutputSuccess(output: AVFoundation.AVAudioSessionPortDescription)
  case setAudioOutputFailure(output: AVFoundation.AVAudioSessionPortDescription)
  case permissionErrorMicrophone
  case permissionErrorCamera
  case sessionReady
  case sessionEnded
  case sessionError(error: DigitalHumanSDK.DHError)
  case sessionLive
  case sessionPaused
  case sessionResumed
  case recordingStarted
  case recordingStopped
  case recordingError
  case connected
  case connectionLost
  case warning(msg: Swift.String)
  case webRtcData(data: Foundation.Data)
  case webRtcStats(stats: [[Swift.String : Swift.String]])
  case logMessage(msg: Swift.String)
  case onlineStatusUpdateMessage(connected: Swift.Bool)
  case micActivity(level: Swift.Float)
  case micActivityError(error: Swift.Error)
  case webRTC(message: Swift.String)
  case sessionInfo(message: Swift.String?)
}
extension DigitalHumanSDK {
  public func requestWebRTCStats()
}
extension DigitalHumanSDK {
  public func speakerOn()
  public func speakerOff()
  public func unmuteAudio()
  public func muteAudio()
}
@objc public class DigitalHumanSDK : ObjectiveC.NSObject {
  public var debugRetryEnabled: Swift.Bool
  public var connected: Swift.Bool
  public var version: Swift.String
  weak public var avatarDelegate: DigitalHumanSDK.DHTextDelegate?
  weak public var messageDelegate: DigitalHumanSDK.DHMessageDelegate?
  public var loggingLevel: DigitalHumanSDK.DHLoggingLevel
  public var currentCamera: AVFoundation.AVCaptureDevice.Position? {
    get
  }
  public var onlineStatus: Swift.Bool
  public var micActivityMessages: Swift.Bool {
    get
    set
  }
  public func getSessionId() -> Swift.String?
  public init(options: DigitalHumanSDK.DHOptions)
  public init(tokenOptions: DigitalHumanSDK.DHTokenOptions)
  public func startSession(sessionId: Swift.String)
  public func getRemoteVideoView(frame: CoreGraphics.CGRect) -> UIKit.UIView?
  public func getLocalVideoPreviewView(frame: CoreGraphics.CGRect) -> UIKit.UIView?
  @objc override dynamic public init()
  @objc deinit
}
extension DigitalHumanSDK {
  public func pauseSession()
  public func resumeSession()
  public func endSession()
}
extension DigitalHumanSDK {
  public func startRecording()
  public func stopRecording()
}
extension DigitalHumanSDK {
  public func getCameraPositionsAvailable() -> [AVFoundation.AVCaptureDevice.Position]
  public func setCamera(_ position: AVFoundation.AVCaptureDevice.Position)
  public func flipCameraPreview()
}
extension DigitalHumanSDK {
  public func sendTranscript(text: Swift.String)
  public func callWelcome()
  public func stopSpeaking()
}
extension DigitalHumanSDK {
  public func getCurrentMicInput() -> AVFoundation.AVAudioSessionPortDescription?
  public func getMicInputsAvailable() -> [AVFoundation.AVAudioSessionPortDescription]
  public func setMicInput(_ input: AVFoundation.AVAudioSessionPortDescription)
}
extension DigitalHumanSDK {
  public func getCurrentAudioOutput() -> AVFoundation.AVAudioSessionPortDescription?
  public func getAudioOutputs() -> [AVFoundation.AVAudioSessionPortDescription]
  public func setAudioOutput(_ output: AVFoundation.AVAudioSessionPortDescription)
}
extension DigitalHumanSDK.DHRTCConfig : Swift.Equatable {}
extension DigitalHumanSDK.DHRTCConfig : Swift.Hashable {}
extension DigitalHumanSDK.DHRTCConfig : Swift.RawRepresentable {}
extension DigitalHumanSDK.DHLoggingLevel : Swift.Equatable {}
extension DigitalHumanSDK.DHLoggingLevel : Swift.Hashable {}
extension DigitalHumanSDK.DHLoggingLevel : Swift.RawRepresentable {}

Digital Human SDK for iOS 🚀
==========================

Overview
-------
The Digital Human SDK for iOS is a library that allows the Uneeq Digital Humans service to integrate with your iOS apps.

For more information visit: [digitalhumans.com](https://www.digitalhumans.com)

Developer Reference Documentation
------
[Reference Docs](docs/)

Installation
------

#### Example projects

Example projects are provided in the [Examples](Examples/)     directory. These are a "kitchen sink" demonstration of the whole of the SDK, and a quick start project with the bare minimum configuration to get started.

#### Xcode project configuration requirements

##### Bitcode
Your Xcode project needs to have `bitcode` disabled.
![Bitcode Flag](docs/img/bitcode.png "Bitcode Flag").

#### Swift

We support Swift 5.1.2 and Xcode 11.2 onwards with this SDK. Whilst the SDK can be called by Objective C, all examples are Swift based.

Cocoapods 1.9.1 onwards is required, due to the SDK using the xcframework format.

##### Permissions

The permissions required for the SDK to run are access to the user's camera, and microphone. Without these permissions a Digital Human chat session cannot be started.
* These are "Privacy - Microphone Usage Description" (`NSMicrophoneUsageDescription`) and "Privacy - Camera Usage Description" (`NSCameraUsageDescription`). 

Your application should request these permissions, before trying to connect to the Digital Humans service. For example:

```
func checkPermissions() {
    // Microphone permission
    if AVAudioSession.sharedInstance().recordPermission == .undetermined {
        // Request permission
        AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
            // Handle granted
        })
    }

    // Camera permission
    if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            // Handle granted
        })
    }
}
```

##### CocoaPods

The SDK is installed via Cocoapods, linking directly to this git repository. You do this by adding a line to your target's pods section in your `Podfile`, like so:

```
pod 'DigitalHumanSDK', :git => 'https://gitlab.com/uneeq-oss/ios-sdk.git'
```
Then install via Cocoapods
```
pod install
```

##### Adding the SDK to your code

Import the SDK, like so:

```
import DigitalHumanSDK
```

An SDK object is created by either with a clientSecret, or with a tokenId (single use token).

**Initializing with a client secret**

initialise with a `DHOptions` struct. This struct contains your Digital Humans credentials.

```
let options = DHOptions(workspaceId: "AWORKSPACEID",
                        clientSecret: "ACLIENTSECRET",
                        url: "AREGIONURL")

let digitalHumanSDK = DigitalHumanSDK(options: options)
```

**Initializing with a tokenId**

initialise with a `DHTokenOptions` struct. This struct contains your Digital Humans credentials.

```
let options = DHTokenOptions(workspaceId: "AWORKSPACEID",
                        tokenId: "ASINGLEUSETOKEN",
                        url: "AREGIONURL")

let digitalHumanSDK = DigitalHumanSDK(tokenOptions: options)
```

When initializing, you'll need to provide the URL where your Digital Human is provisioned. If you have access to the [UneeQ Creator](https://creator.uneeq.io/), you can find the correct URL in the Deploy > Build your own area. Otherwise, please contact your Customer Success representative if you're unsure of the [region](https://docs.uneeq.io/#/js_sdk?id=region-urls) in which your Digital Human has been provisioned."

Once you have an SDK object, you should retain it for the entirety of the duration of the call to the Digital Humans service.

In our example projects we illustrate a single ViewController approach in the Quick Start project, and a dependency injected approach across multiple viewcontrollers in the Kitchen Sink project.


##### Receiving messages from the service

The Digital Humans SDK communicates to your application via delegation. There are two delegates that can be set on a `DigitalHumanSDK` object: 

* `messageDelegate`, a `DHMessageDelegate` property. This transmits DHMessage enums back to your app, to communicate the state of your session with the Digital Human service.
* `avatarDelegate`, a `DHTextDelegate` property. This transmits text strings back to your app, with the text spoken by the Digital Human you connect to with the SDK.


Your application needs to set itself as the message delegate for the SDK. Your ViewController or other manager object will need to conform to `DHMessageDelegate` in order to do so. The protocol is simple, with only one function needing to be added:

```
func digitalHumanMessage(_ message: DHMessage) {
}
```

##### Calling the service and starting a session

To start a session, call `startSession(sessionId: "A-SESSION-ID")` on your `DigitalHumanSDK` object. Your sessionId supplied should be a unique identifier for the call.

If a Digital Human is available to speak to, then the service will connect and a `sessionLive` message shall be sent back to the messageDelegate. If not, then an `avatarUnavailable` message is sent. Your application should respond to this and set the appropriate UI to communicate this to the user. 

##### Session live
When the session is live, you can then display the call to the user.


The SDK will send a  `sessionLive` message back to the delegate. Your application should display the remote video view and the local video view. The remote video view displays the Digital Human, and the local view 

*  To retrieve the remote video view from the SDK, call `.getRemoteVideoView()` on your DigitalHumanSDK object.
* To retrieve the remote video view from the SDK, call `.getLocalVideoPreviewView(frame: containingview.frame)` on your DigitalHumanSDK object.

Upon receiving a `sessionEnded` message, your application should remove the remote and local video views from your view hierarchy.

In our Quick Start project we do this like so.

```
func digitalHumanMessage(_ message: DHMessage) {
    print("digitalHumanMessage received: \(message)")

    switch message {
    case .sessionReady:
        // Get the remote video view for the Digital Human from the SDK
        if let view = self.digitalHumanSDK.getRemoteVideoView(frame: self.remoteView.frame) {
            self.remoteView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                view.leadingAnchor.constraint(equalTo: self.remoteView.leadingAnchor),
                view.trailingAnchor.constraint(equalTo: self.remoteView.trailingAnchor),
                view.topAnchor.constraint(equalTo: self.remoteView.topAnchor),
                view.bottomAnchor.constraint(equalTo: self.remoteView.bottomAnchor)
            ])
        }

        // Get the local video view for the Digital Human from the SDK
        if let view = self.digitalHumanSDK.getLocalVideoPreviewView(frame: self.localView.frame) {
            self.localView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                view.leadingAnchor.constraint(equalTo: self.localView.leadingAnchor),
                view.trailingAnchor.constraint(equalTo: self.localView.trailingAnchor),
                view.topAnchor.constraint(equalTo: self.localView.topAnchor),
                view.bottomAnchor.constraint(equalTo: self.localView.bottomAnchor)
            ])
        }
    case .sessionEnded:
        for subview in self.remoteView.subviews {
        subview.removeFromSuperview()
        }

        for subview in self.localView.subviews {
        subview.removeFromSuperview()
        }
    default:
        break
    }
}
```

##### Talking to the Digital Human

The key mechanism for talking to the Digital Human is via voice from the user's microphone. Other methods are detailed in the [developer reference docs](docs/).

To do this, call startRecording on your `DigitalHumanSDK` object:
```
digitalHumanSDK.startRecording()
```

To stop talking to the Digital Human:
```
digitalHumanSDK.stopRecording()
```
This should be treated like a 'walkie talkie' with the Digital Human providing a response to what has been said, once `stopRecording` is called.

##### Finishing the session

To stop the call with the Digital Human, your application should call `stopSession` on the `DigitalHumanSDK` object.

```
digitalHumanSDK.endSession()
```

The SDK will then send back a `sessionEnded` message to the delegate, after disconnecting the call.

License
------
See https://digitalhumans.com

Pod::Spec.new do |s|  
    s.name              = 'DigitalHumanSDK'
    s.version           = '1.3.7'
    s.summary           = 'Digital Humans iOS SDK, enables connection to the digital humans service'
    s.homepage          = 'https://www.digitalhumans.com'

    s.author            = { 'Name' => 'ios@digitalhumans.com' }
    s.license = {:type => 'commercial', :text =>'Please refer to our privacy policy and terms of service at http://digitalhumans.com'}

    s.platform          = :ios
    s.source       = { :git => 'https://gitlab.com/uneeq-oss/ios-sdk', :tag => '1.3.7' }

    s.ios.deployment_target = '10.0'
    s.vendored_frameworks = 'DigitalHumanSDK.xcframework'

    s.dependency 'Alamofire', '~> 4.9.1'
    s.dependency 'GoogleWebRTC', '~> 1.1.29400'
    s.dependency "StompClientLib", '1.3.1'
end  
